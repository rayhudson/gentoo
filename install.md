#### Set the keyboard layout

The default [console keymap](https://wiki.archlinux.org/title/Linux_console/Keyboard_configuration) is [US](https://en.wikipedia.org/wiki/File:KB_United_States-NoAltGr.svg). Available layouts can be listed with : 
```
# ls /usr/share/kbd/keymaps/**/*.map.gz
```
To modify the layout, append a corresponding file name to [loadkeys(1)](https://man.archlinux.org/man/loadkeys.1), omitting path and file extension. For example, to set a [British](https://commons.wikimedia.org/wiki/File:KB_United_Kingdom.svg) keyboard layout :
```
# loadkeys uk
```
[Console fonts](https://wiki.archlinux.org/title/Linux_console#Fonts) are located in `/usr/share/kbd/consolefonts/` and can likewise be set with [setfont(8)](https://man.archlinux.org/man/setfont.8). 

#### Verify the boot mode
To verify the boot mode, list the [efivars](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface#UEFI_variables) directory :
```
# ls /sys/firmware/efi/efivars
```
If the command shows the directory without error, then the system is booted in UEFI mode. If the directory does not exist, the system may be booted in [BIOS](https://wikipedia.org/wiki/BIOS) (or [CSM](https://wikipedia.org/wiki/Unified_Extensible_Firmware_Interface#CSM_booting)) mode. If the system did not boot in the mode you desired, refer to your motherboard's manual. 

#### Connect to the internet
To set up a network connection in the live environment, go through the following steps:
* Ensure your [network interface](https://wiki.archlinux.org/title/Network_configuration#Network_interfaces) is listed and enabled, for example with [ip-link(8)](https://man.archlinux.org/man/ip-link.8): 
 ```
 # ip link
 ```
* For wireless and WWAN, make sure the card is not blocked with [rfkill](https://wiki.archlinux.org/title/Network_configuration/Wireless#Rfkill_caveat).
* Connect to the network:
  * [Ethernet](https://wikipedia.org/wiki/Ethernet) - plug in the cable.
  * [Wi-Fi](https://wikipedia.org/wiki/Wi-Fi) - authenticate to the wireless network using **net-setup**.
* The connection may be verified with [ping](https://wikipedia.org/wiki/Ping_(networking_utility)). 
```
# ping https://gentoo.org
```
#### Update the system clock
Use [ntpd](https://man.archlinux.org/man/ntpd.1.en) to ensure the system clock is accurate :
```
# ntpd -q -g
```

#### Partition the disks
When recognized by the live system, disks are assigned to a [block device](https://wiki.archlinux.org/title/Device_file#Block_devices) such as `/dev/sda`, `/dev/nvme0n1` or `/dev/mmcblk0`. To identify these devices, use [lsblk](https://wiki.archlinux.org/title/Device_file#lsblk) or [fdisk](https://man.archlinux.org/man/fdisk.8.en).
<br>Results ending in `rom`, `loop` or `airoot` may be ignored :</br>
```
# fdisk -l
```
Use [sgdisk](https://man.archlinux.org/man/sgdisk.8.en) to destroy all existing partitions :
```
# sgdisk -Z /dev//dev/the_disk_to_be_partitioned
```
Use the [wipe](https://man.archlinux.org/man/wipe.1) command to destroy all data on the disk :
```
# wipe -a /dev//dev/the_disk_to_be_partitioned
```
<br> The following [partitions](https://wiki.archlinux.org/title/Partitioning) are **required** for a chosen device:</br>
* One partition for the encrypted [root directory](https://en.wikipedia.org/wiki/Root_directory) /.
* For booting in [UEFI](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface) mode: an [EFI](https://wiki.archlinux.org/title/EFI_system_partition) system partition. 
<br>For this [Gentoo](https://en.wikipedia.org/wiki/Gentoo_Linux) installation guide we will be using [btrfs](https://en.wikipedia.org/wiki/Btrfs) on a [LUKS](https://wikipedia.org/wiki/Linux_Unified_Key_Setup) encrypted partition.</br>
Use [fdisk](https://wiki.archlinux.org/title/Fdisk), [parted](https://wiki.archlinux.org/title/Parted) or [gdisk](https://wiki.archlinux.org/title/GPT_fdisk) to modify partition tables. For example :
```
# gdisk /dev/the_disk_to_be_partitioned
```
##### Partition layout
 Mount point |   Partition  | [Partition Type](https://wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs) | Suggested size |
------------ | ------------ | ----------------------------------------------------------------------------------------- | --------------
/mnt/gentoo/boot/efi | /dev/efi_system_partition | [EFI system partition](https://wiki.archlinux.org/title/EFI_system_partition) | 256M |
/mnt/gentoo/boot | /dev/boot_partition | Boot Partition | 512M |
/mnt/gentoo/ | /dev/root_partition | [Root partition](https://wikipedia.org/wiki/Root_directory) | Everything that's left |

#### Format the partitions.
Once the partitions have been created, each newly created partition must be formatted with an appropriate [file system](https://wiki.archlinux.org/title/File_systems). For this guide, create an [Ext4](https://wikipedia.org/wiki/Ext4) file system on `/dev/root_partition`, run :
```
# mkfs.ext4 -L root /dev/root_partition
```
Format the [esp](https://wiki.archlinux.org/title/EFI_system_partition) partition :
```
# mkfs.vfat -n "EFI System" /dev/esp_partition
```
[Dm-crypt](https://wikipedia.org/wiki/Dm-crypt) is a transparent [disk encryption](https://wikipedia.org/wiki/Disk_encryption) subsystem in [Linux kernel](https://wikipedia.org/wiki/Linux_kernel) versions 2.6 and later and in [DragonFly BSD](https://wikipedia.org/wiki/DragonFly_BSD). It is part of the [device mapper](https://wikipedia.org/wiki/Device_mapper) (dm) infrastructure, and uses cryptographic routines from the kernel's [Crypto API](https://wikipedia.org/wiki/Crypto_API_(Linux)). Unlike its predecessor [cryptoloop](https://wikipedia.org/wiki/Cryptoloop), dm-crypt was designed to support advanced modes of operation, such as [XTS](https://wikipedia.org/wiki/Disk_encryption_theory#XTS), [LRW](https://wikipedia.org/wiki/Disk_encryption_theory#Liskov,_Rivest,_and_Wagner_(LRW)) and [ESSIV](https://wikipedia.org/wiki/Disk_encryption_theory#Encrypted_salt-sector_initialization_vector_(ESSIV)) (see [disk encryption theory](https://wikipedia.org/wiki/Disk_encryption_theory) for further information), in order to avoid [watermarking attacks](https://wikipedia.org/wiki/Watermarking_attack). In addition to that, [dm-crypt](https://wikipedia.org/wiki/Dm-crypt) also addresses some reliability problems of [cryptoloop](https://wikipedia.org/wiki/Cryptoloop). Make sure [dm-crypt](https://wikipedia.org/wiki/Dm-crypt) is enabled as a [kernel module](https://wiki.archlinux.org/title/Kernel_module) :
```
# modprobe dm-crypt
# modprobe dm-mod
```
#### Setting up encryption on the [root partition](https://en.wikipedia.org/wiki/Root_directory) :
```
# cryptsetup luksFormat --type luks2 --use-random -S 1 -s 512 -h sha512 -i 5000 /dev/root_partition
# cryptsetup open /dev/root_partition
```
#### Mount the file systems
[Mount](https://wiki.archlinux.org/title/File_systems#Mount_a_file_system) the root volume to `/mnt/gentoo`. For example, if the [root](https://wikipedia.org/wiki/Root_directory) volume is `/dev/root_partition`.

<br>Format the encrypted [root partition](https://wikipedia.org/wiki/Root_directory) :</br>
```
# mkfs.btrfs -L root /dev/mapper/gentoo
```
[Mounting](https://wiki.archlinux.org/title/File_systems#Mount_a_file_system) the encrypted [btrfs](https://wikipedia.org/wiki/Btrfs) partition :
```
# mount -t btrfs /dev/mapper/gentoo /mnt/gentoo
```
Creating the [btrfs](https://wikipedia.org/wiki/Btrfs) [subvolumes](https://wiki.archlinux.org/title/User:I2Oc9/Btrfs_subvolumes) :
```
# cd /mnt/gentoo
# btrfs subvolume create @
# btrfs subvolume create @home
# btrfs subvolume create @swap
# btrfs subvolume create @snapshots
```
Mounting the [btrfs](https://wikipedia.org/wiki/Btrfs) [subvolumes](https://wiki.archlinux.org/title/User:I2Oc9/Btrfs_subvolumes) :
```
# cd /
# umount -R /mnt/gentoo
# mount -t btrfs -o subvol=@ /dev/mapper/gentoo /mnt/gentoo
# mkdir /mnt/gentoo/home
# mount -t btrfs -o subvol=@home /dev/mapper/gentoo /mnt/gentoo/home
# mkdir /mnt/gentoo/swap
# mount -t btrfs -o subvol=@swap /dev/mapper/gentoo /mnt/gentoo/swap
# mkdir /mnt/gentoo/snapshots
# mount -t btrfs -o subvol=@snapshots /dev/mapper/gentoo /mnt/gentoo/snapshots
```
Mounting and setting up the [GRUB](https://wiki.archlinux.org/title/GRUB) partitions :
```
# mkdir /mnt/gentoo/boot
# mount /dev/( Disk Name ) 2 /mnt/gentoo/boot
# mkdir /mnt/gentoo/boot/efi
# mount /dev/( Disk Name ) 1 /mnt/gentoo/boot/efi
```
Creating and setting up a [swap](https://wiki.archlinux.org/title/Swap) file :
```
# cd /mnt/gentoo
# touch /mnt/gentoo/swap/swapfile
# chattr +C /mnt/gentoo/swap/swapfile
# fallocate /mnt/gentoo/swap/swapfile -l ( Swap size: Your ram size or otherwise. Examples: 1024M, 64M, 8G, )
# chmod 0600 /mnt/gentoo/swap/swapfile
# mkswap /mnt/gentoo/swap/swapfile
# swapon /mnt/gentoo/swap/swapfile
```
#### Installing a stage tarball

Download the latest stage3 tarball :
```
# links https://www.gentoo.org/downloads/mirrors/
```
Choose a mirror preferably with [https](https://wikipedia.org/wiki/HTTPS) and use it:
```
/releases/amd64/autobuilds/
```
After it, you have the choice between:
```
current-stage3-amd64-hardened or
current-stage3-amd64-hardened+nomultilib
```
Choose one and download all the files that match ( .tar.xz , .tar.xz.CONTENTS , .tar.xz.DIGESTS , .tar.xz.DIGESTS.asc ) and quit with pressing 'q'.

##### Verify the stage3 
Download the last gpg key of [Gentoo release](https://wiki.gentoo.org/wiki/Project:RelEng).
```
# gpg --keyserver pool.sks-keyservers.net --recv-key 0x2D182910 
# gpg --verify stage3-amd64-*.DIGESTS.asc
```
```
  gpg: Signature made Fri 05 Dec 2014 02:42:44 AM CET
  gpg:                using RSA key 0xBB572E0E2D182910
  gpg: Good signature from "Gentoo Linux Release Engineering (Automated Weekly Release Key) <releng@gentoo.org>" [unknown]
  gpg: WARNING: This key is not certified with a trusted signature!
  gpg:          There is no indication that the signature belongs to the owner.
  Primary key fingerprint: 13EB BDBE DE7A 1277 5DFD  B1BA BB57 2E0E 2D18 2910
```
Only look for the ``Good signature`` here, you can compare the fingerprint [here](https://www.gentoo.org/downloads/signatures/).
<br>The checksums:</br>
```
# awk '/SHA512 HASH/{getline;print}' stage3-amd64-*.tar.xz.DIGESTS.asc | sha512sum --check
```
```
stage3-amd64-*.tar.xz: OK
stage3-amd64-*.tar.xz.CONTENTS: OK
```
Extract the stage3 archhive that you just downloaded :
```
# tar xvJpf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
```
You can now delete all the files downloaded.
```
# rm stage3*
```
#### Chrooting
Configuring mirrors ( For security reasons, choose only https mirrors ) :
```
# mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf
```
##### Gentoo ebuild repository
Configuring the repository can be done in a few simple steps. First, if it does not exist, create the [repos.conf](https://wiki.gentoo.org/wiki//etc/portage/repos.conf) directory :
```
# mkdir --parents /mnt/gentoo/etc/portage/repos.conf
```
Next, copy the Gentoo repository configuration file provided by Portage to the (newly created) [repos.conf](https://wiki.gentoo.org/wiki//etc/portage/repos.conf) directory :
```
# cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
```
Finally, edit the file and add the lines that don't exist :
```
/mnt/etc/portage/repos.conf/gentoo.conf
```
```
[DEFAULT]
sync-allow-hardlinks = yes

[gentoo]
location = /var/db/repos/gentoo
sync-type = webrsync
#sync-type = rsync
```
``webrsync`` will build the portage dir ``/var/db/repos/gentoo``.

#### Copy DNS info
```
# cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
```
##### Mounting the necessary filesystems
In a few moments, the Linux [root](https://wikipedia.org/wiki/Root_directory) will be changed towards the new location. To make sure that the new environment works properly, certain filesystems need to be made available there as well.

The filesystems that need to be made available are:

* [/proc/](https://wiki.archlinux.org/title/Procfs) which is a pseudo-filesystem (it looks like regular files, but is actually generated on-the-fly) from which the Linux kernel exposes information to the environment
* /sys/ which is a pseudo-filesystem, like /proc/ which it was once meant to replace, and is more structured than [/proc/](https://wiki.archlinux.org/title/Procfs)
* /dev/ is a regular file system, partially managed by the Linux device manager (usually **udev**), which contains all device files

The /proc/ location will be mounted on /mnt/gentoo/proc/ whereas the other two are bind-mounted. The latter means that, for instance, /mnt/gentoo/sys/ will actually be /sys/ (it is just a second entry point to the same filesystem) whereas /mnt/gentoo/proc/ is a new mount (instance so to speak) of the filesystem :
```
# mount --types proc /proc /mnt/gentoo/proc
# mount --rbind /sys /mnt/gentoo/sys
# mount --make-rslave /mnt/gentoo/sys
# mount --rbind /dev /mnt/gentoo/dev
# mount --make-rslave /mnt/gentoo/dev
```
Now that all partitions are initialized and the base environment installed, it is time to enter the new installation environment by chrooting into it. This means that the session will change its root (most top-level location that can be accessed) from the current installation environment (installation CD or other installation medium) to the installation system (namely the initialized partitions). Hence the name, change root or chroot : 
```
# chroot /mnt/gentoo /bin/bash
(chroot) # source /etc/profile
(chroot) # export PS1="(chroot) ${PS1}"
```
#### Configuring Portage
The first time using webrsync may take a while, especially on a low-cost network :
```
(chroot) # emaint sync --auto
```
Finalize using gpg keys on portage by choosing rsync.
Edit the gentoo.conf file :
```
/etc/portage/repos.conf/gentoo.conf
```
Modify the ``sync-type`` :
```
[gentoo]
#sync-type = webrsync
sync-type = rsync
```
Re-sync portage with the updated gentoo.conf file :
```
(chroot) #  emaint sync --auto
```
Now that we've grabed all the lastest ebuilds, before we recompile each package, we'll have to finish configuring our processor correctly.

#### Flags
The full list of supported flags can be found with at ``/proc/cpuinfo` but we'll have to install ``app-portage/cpuid2cpuflags`` to find those used by gentoo :
```
(chroot) # emerge -av app-portage/cpuid2cpuflags
(chroot) # echo "*/* $(cpuid2cpuflags)" >> /etc/portage/package.use/00cpuflags
```
Add directory's and files for portage :
```
(chroot) # mkdir -p -v /etc/portage/package.use
(chroot) # mkdir -p -v /etc/portage/package.accept_keywords
(chroot) # mkdir -p -v /etc/portage/package.unmask
```
And for files :
```
(chroot) # touch /etc/portage/package.use/zzz_via_autounmask
(chroot) # touch /etc/portage/package.accept_keywords/zzz_via_autounmask
(chroot) # touch /etc/portage/package.unmask/zzz_via_autounmask
```
These files serve portage so ( for example ) you make an update which requires new use flags or masks an old package.
##### CFLAGS and other options.
Find the arch of your processor :
```
(chroot) # gcc -c -Q -march=native --help=target | grep march -march=silvermont
```
Create two files, they'll serve us to find good options :
```
(chroot) # touch native.cc march.cc
```
Compile them and replace the march with the result above :
```
(chroot) # gcc -fverbose-asm -march=native native.cc -S
(chroot) # gcc -fverbose-asm -march=silvermont march.cc -S
(chroot) # sed -i 1,/options\ enabled/d march.s
(chroot) # sed -i 1,/options\ enabled/d native.s
(chroot) # diff march.s native.s
```
```
< # -maccumulate-outgoing-args -maes -malign-stringops -mcx16
---
> # -maccumulate-outgoing-args -malign-stringops -mcx16 -mfancy-math-387
```
We'll compare the two lines, look at ``-maes`` on the first line. Not present on second line, we need to disable it, replace ``-m`` with ``-mno-``. We'll do it until the ouput return's nothing :
```
(chroot) # gcc -fverbose-asm -march=silvermont -mno-aes march.cc -S
(chroot) # sed -i 1,/options\ enabled/d march.s
(chroot) # diff march.s native.s
```
```
< # -mpclmul -mpopcnt -mpush-args -mred-zone -msahf -msse -msse2 -msse3
- - -
> # -mpclmul -mpopcnt -mprfchw -mpush-args -mrdrnd -mred-zone -msahf -msse
```
The intruder is ``-mprfchw`` on the second line which is enabled, we'll have to add it on a first line :
```
(chroot) # gcc -fverbose-asm -march=silvermont -mno-aes -mprfchw march.cc -S
(chroot) # sed -i 1,/options\ enabled/d march.s
(chroot) # diff march.s native.s
```
```
< # -mpclmul -mpopcnt -mprfchw -mpush-args -mred-zone -msahf -msse -msse2
- - -
> # -mpclmul -mpopcnt -mprfchw -mpush-args -mrdrnd -mred-zone -msahf -msse
```
Now, it's ``-mrdrnd`` :
```
(chroot) # gcc -fverbose-asm -march=silvermont -mno-aes -mprfchw -mrdrnd march.cc -S
(chroot) # sed -i 1,/options\ enabled/d march.s
(chroot) # diff march.s native.s
```
If the output return nothing, that mean than you've finally finished configuring gcc.
<br>Add result to ``/etc/portage/make.conf`` with ``-O2 -pipe`` :</br>
```
(chroot) # cat >> /etc/portage/make.conf
COMMON_FLAGS="-march=silvermont -mno-aes -mprfchw -mrdrnd -O2 -pipe"
^D
```
Your processor is now correctly configured.

#### Others CFlags


GCC can have additionnal flags, especially to limit the behavior of [Meltown & Spectre](https://meltdownattack.com/), or to limit an attack based on the stack, etc...

<br>According to this post by [Red Hat](https://developers.redhat.com/blog/2018/03/21/compiler-and-linker-flags-gcc), we can add some flags to gcc ( tested on my last install ) like :</br>

* ``-fstack-protector-strong``, stack smashing protector
* ``-Wl,-z,relro`` read-only segments after relocation
* ``-Wl,-z,now``, disable lazy binding

But do not enable all the flags as some may make the system unstable. Don't add ``-fpie -Wl,-pie`` or ``-fexceptions`` #2 globally too, many packages wont compile with this enabled and grub will fail to compile with ``-D_FORTIFY_SOURCE=2``.

So in my case, the CFLAGS line looks like this :
```
COMMON_FLAGS="-march=silvermont -mno-aes -mprfchw -mrdrnd -O2 -pipe -fstack-protector-strong -Wl,-z,relro -Wl,-z,now"
```
#### Recompile the whole system
```
(chroot) # source /etc/profile && env-update 
(chroot) # emerge --ask --update --deep --newuse --with-bdeps=y @world 
```
When installation has finished, delete older packages with :
```
(chroot) # emerge -av --depclean
(chroot) # eclean -d distfiles
```
#### Gentoolkit
Install gentoolkit because it's going to help us very soon :
```
(chroot) # emerge -av gentoolkit
```
We'll add some use flags with euse:
<br>If you've used cryptsetup, add the use flag ``cryptsetup`` globally :</br>
```
(chroot) # euse -E cryptsetup
```
And for the rest, add pie and audit :
```
(chroot) # euse -E urandom pie audit
```
#### Choose the correct profiles
To view all available profiles :
```
(chroot) # eselect profile list
```
Activate the linux-hardened profile :
```
(chroot) # eselect profile set ( Profile number of linux-hardened )
```
#### Gentoo sources
Install the kernel source :
```
(chroot) # emerge -av --oneshot sys-kernel/gentoo-sources
```
And make a symbolink with ``eselect`` tool :
```
(chroot) # eselect kernel list
(chroot) # eselect kernel set 1
```
On the next step, we're going to compile the kernel.

#### Kernel configuration and compilation
In [computing](https://wikipedia.org/wiki/Computing), [firmware](https://wikipedia.org/wiki/Firmware) is a specific class of [computer software](https://wikipedia.org/wiki/Computer_software) that provides the low-level control for a device's specific [hardware](https://wikipedia.org/wiki/Computer_hardware). Firmware, such as the [BIOS](https://wikipedia.org/wiki/BIOS) of a personal computer may contain only elementary basic functions of a device and may only provide services to higher-level software. For less complex devices, firmware act as the device's complete [operating system](https://wikipedia.org/wiki/Operating_system), performing all control, monitoring and data manipulation functions. Typical examples of devices containing firmware are [embedded systems](https://wikipedia.org/wiki/Embedded_systems), consumer appliances, computers, and [computer peripherals](https://wikipedia.org/wiki/Computer_peripheral) : 
```
(chroot) # echo "sys-kernel/linux-firmware @BINARY-REDISTRIBUTABLE" | tee -a /etc/portage/package.license
```
In this [gentoo](https://wikipedia.org/wiki/Gentoo_Linux) installation guide, to make configuring the [linux kernel](https://wikipedia.org/wiki/Linux_kernel) easier we're going to use a script called bask to build a secure and effective kernel : 
```
(chroot) # cd
(chroot) # git clone https://github.com/szorfein/bask
(chroot) # cd bask 
```
We're going to have to add support for kmod & lzma by updating the portage use flags and installing kmod :
```
(chroot) # euse -p sys-apps/kmod -E lzma
(chroot) # emerge -av kmod
```
Start by applying the base. Recommended by [KSPP](https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings) and [ClipOS](https://docs.clip-os.org/clipos/kernel.html) :
```
(chroot) # ./bask.sh -b
```
[Nftables](https://wiki.archlinux.org/title/nftables) is the modern [Linux kernel](https://wikipedia.org/wiki/Linux_kernel) packet classification framework. New code should use it instead of the legacy {ip,ip6,arp,eb}_tables ([xtables](https://man.archlinux.org/man/core/iptables/xtables-legacy.8.en)) infrastructure. For existing codebases that have not yet converted, the legacy [xtables](https://man.archlinux.org/man/core/iptables/xtables-legacy.8.en) infrastructure is still maintained as of 2021. Automated tools assist the [xtables](https://man.archlinux.org/man/core/iptables/xtables-legacy.8.en) to [nftables](https://wiki.archlinux.org/title/nftables) conversion process : 
```
(chroot) # ./bask.sh -a nftables
```
The ``cryptsetup`` command-line interface, by default, does not write any headers to the encrypted volume, and hence only provides the bare essentials: encryption settings have to be provided every time the disk is mounted (although usually employed with automated scripts), and only one [key](https://wikipedia.org/wiki/Key_(cryptography)) can be used per volume; the [symmetric encryption](https://wikipedia.org/wiki/Symmetric-key_algorithm) key is directly derived from the supplied [passphrase](https://wikipedia.org/wiki/Passphrase) : 
```
(chroot) # ./bask.sh -a cryptsetup
```
[KVM](https://wiki.archlinux.org/title/KVM) converts Linux into a type-1 (bare-metal) [hypervisor](https://en.wikipedia.org/wiki/Hypervisor). All [hypervisors](https://wikipedia.org/wiki/Hypervisor) need some operating system-level components—such as a [memory manager](https://wikipedia.org/wiki/Memory_management_(operating_systems)), [process scheduler](https://wikipedia.org/wiki/Scheduling_(computing)), [input/output](https://wikipedia.org/wiki/Input/output) (I/O) stack, [device drivers](https://wikipedia.org/wiki/Device_driver), [security manager](https://wikipedia.org/wiki/Security_management), a [network stack](https://wikipedia.org/wiki/Protocol_stack), and more—to run [VMs](https://wikipedia.org/wiki/Virtual_machine). [KVM](https://wiki.archlinux.org/title/KVM) has all these components because it’s part of the [Linux kernel](https://wikipedia.org/wiki/Linux_kernel). Every [VM](https://wikipedia.org/wiki/Virtual_machine) is implemented as a regular Linux process, scheduled by the standard Linux [scheduler](https://wikipedia.org/wiki/Scheduling_(computing)), with dedicated [virtual hardware](https://wikipedia.org/wiki/Hardware_virtualization) like a [network card](https://wikipedia.org/wiki/Network_interface_controller), [graphics adapter](https://wikipedia.org/wiki/Video_card), [CPU(s)](https://wikipedia.org/wiki/Central_processing_unit), [memory](https://wikipedia.org/wiki/Computer_memory), and [disks](https://wikipedia.org/wiki/Disk_storage) : 
```
(chroot) # /bask.sh -a "kvm-host"
```
[Wi-Fi](https://wikipedia.org/wiki/Wi-Fi) is a family of [wireless network](https://wikipedia.org/wiki/Wireless_network) [protocols](https://wikipedia.org/wiki/Communication_protocol), based on the [IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11) family of standards, which are commonly used for [local area networking](https://wikipedia.org/wiki/Wireless_LAN) of devices and [Internet](https://wikipedia.org/wiki/Internet) access, allowing nearby digital devices to exchange data by [radio waves](https://wikipedia.org/wiki/Radio_wave). These are the most widely used computer networks in the world, used globally in [home and small office networks](https://wikipedia.org/wiki/Small_office/home_office) to link [desktop](https://wikipedia.org/wiki/Desktop_computer) and [laptop](https://wikipedia.org/wiki/Laptop) computers, [tablet computers](https://wikipedia.org/wiki/Tablet_computer), [smartphones](https://wikipedia.org/wiki/Smartphone), [smart TVs](https://wikipedia.org/wiki/Smart_TV), [printers](https://wikipedia.org/wiki/Printer_(computing)), and [smart speakers](https://wikipedia.org/wiki/Smart_speaker) together and to a [wireless router](https://wikipedia.org/wiki/Wireless_router) to connect them to the Internet, and in [wireless access points](https://wikipedia.org/wiki/Wireless_access_point) in public places like coffee shops, hotels, libraries and airports to provide the public Internet access for mobile devices :
```
(chroot) # ./bask.sh -a wifi
```
[Advanced Linux Sound Architecture](https://wikipedia.org/wiki/Advanced_Linux_Sound_Architecture) (ALSA) is a [software framework](https://wikipedia.org/wiki/Software_framework) and part of the [Linux kernel](https://wikipedia.org/wiki/Linux_kernel) that provides an [application programming interface](https://wikipedia.org/wiki/Application_programming_interface) (API) for [sound card](https://wikipedia.org/wiki/Sound_card) [device drivers](https://wikipedia.org/wiki/Device_driver). Some of the goals of the ALSA project at its inception were automatic configuration of sound-card hardware and graceful handling of multiple sound devices in a system. ALSA is released under the [GNU General Public License](https://wikipedia.org/wiki/GNU_General_Public_License) (GPL) and the [GNU Lesser General Public License](https://wikipedia.org/wiki/GNU_Lesser_General_Public_License)(LGPL). The sound servers [PulseAudio](https://wikipedia.org/wiki/PulseAudio), [JACK](https://wikipedia.org/wiki/JACK_Audio_Connection_Kit) (low-latency professional-grade audio editing and mixing) and [PipeWire](https://wikipedia.org/wiki/PipeWire), the higher-level abstraction APIs [OpenAL](https://wikipedia.org/wiki/OpenAL), [SDL audio](https://wikipedia.org/wiki/Simple_DirectMedia_Layer#Subsystems), etc. work on top of ALSA and implemented sound card device drivers. On Linux systems, ALSA succeeded the older [Open Sound System](https://wikipedia.org/wiki/Open_Sound_System) (OSS) :

```
(chroot) # ./bask.sh -a sound
```
#### Compile the kernel
With the configuration now done, it is time to compile and install the kernel. Now start the compilation process :
```
(chroot) # cd /usr/src/linux
(chroot) # make -j$(nproc)
```

